#!/usr/bin/env sh

#
# ~/.profile
#

# load local profile
if [ -f ~/.profile.local ]; then
    source ~/.profile.local
fi

# load crontab
if [ -f ~/.crontab ] && hash crontab 2>/dev/null; then
    crontab ~/.crontab
fi

# include ruby directory if the program exists
BUNDLE_PATH=~/.gem
GEM_HOME=''

if hash ruby 2>/dev/null; then
    GEM_HOME="$(ruby -e 'print Gem.user_dir')"
    PATH="$GEM_HOME/bin:$PATH"
fi

# add path to pkg config
PKG_CONFIG_PATH=/usr/lib/imagemagick6/pkgconfig

# setup android environment variables
ANDROID_HOME=~/Android/Sdk
ANDROID_SDK_ROOT="$ANDROID_HOME"
ANDROID_NDK_ROOT=~/Android/Ndk

# setup n version manager variables
N_PREFIX=~/.n
PREFIX="$N_PREFIX"

# include dotnet tools directory
PATH="$HOME/.dotnet/tools:$PATH"

# include local npm modules directory
PATH="$HOME/.node_modules/bin:$PATH"

# include n version manager directory
PATH="$N_PREFIX/bin:$PATH"

# include local bin directory
PATH="$HOME/bin:$PATH"

# include android tools directory
PATH="$ANDROID_HOME/tools/bin:$PATH"

# set dir colors path
DIR_COLORS="$HOME/.dircolors"

# set default editor
VISUAL=nano

# determine if more preferred editor exists
if hash gedit 2>/dev/null; then
    VISUAL='gedit -w'
elif hash code 2>/dev/null; then
    VISUAL="code -w"
fi

# setup the EDITOR variable
EDITOR="$VISUAL"
GIT_EDITOR="$VISUAL"

#customize bash history
HISTCONTROL=ignoreboth:erasedups
HISTIGNORE='ls:bg:fg:history'

# customize pip settings
PIP_REQUIRE_VIRTUALENV=true
VIRTUAL_ENV_DISABLE_PROMPT=true

# customize npm settings
NPM_CONFIG_PREFIX=~/.node_modules

# customize dotnet settings
DOTNET_CLI_TELEMETRY_OPTOUT=true
DOTNET_ROOT=/opt/dotnet

# determine whether bash is within wsl
WSL=false
if grep -iq 'microsoft' /proc/version; then
    WSL=true
fi

# setup Java-specific environment variables
JAVA_HOME=''
GRADLE_USER_HOME="$HOME/.gradle"

if hash javac 2>/dev/null; then
    JAVA_HOME=$(dirname $(dirname $(readlink -f $(which javac))))
fi

# export environment variables
export ANDROID_HOME
export ANDROID_SDK_ROOT
export ANDROID_NDK_ROOT
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export BUNDLE_PATH
export DIR_COLORS
export DOTNET_CLI_TELEMETRY_OPTOUT
export DOTNET_ROOT
export EDITOR
export GEM_HOME
export GIT_EDITOR
export GRADLE_USER_HOME
export HISTCONTROL
export HISTIGNORE
export JAVA_HOME
export LOCAL_WEBSERVER
export N_PREFIX
export NPM_CONFIG_PREFIX
export OPERATING_SYSTEM
export OPERATING_SYSTEM_RELEASE
export PACKAGE_MANAGER
export PATH
export PIP_REQUIRE_VIRTUALENV
export PKG_CONFIG_PATH
export PREFIX
export VIRTUAL_ENV_DISABLE_PROMPT
export VISUAL
export WSL
