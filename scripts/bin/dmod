#!/usr/bin/env bash

# Copyright 2020 Logan Garcia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

usage() {
    printf 'usage: %s [-h] [-d] [-l]\n\n' "$(basename "$0")"
    printf 'Loads and dumps specific dconf configurations\n\n'
    printf 'optional arguments:\n'
    printf ' -h\tshow this help message and exit\n'
    printf ' -d\tdumps configurations to a specified location\n'
    printf ' -l\tloads configurations from a specified location\n' 1>&2
    exit 1
}

action=''
dir="$HOME/.dconf"

declare -A configurations
configurations+=(['gedit']='/org/gnome/gedit/preferences/' \
                 ['gnome-terminal']='/org/gnome/terminal/legacy/')

# process options
while getopts 'hdl' opt; do
    case "${opt}" in
        h)
            # print the usage
            usage
            ;;
        d)
            # make sure action is empty
            if [[ ! -z "$action" ]]; then
                usage
            fi

            action='dump'
            ;;
        l)
            # make sure action is empty
            if [[ ! -z "$action" ]]; then
                usage
            fi

            action='load'
            ;;
        *)
            # print the usage
            usage
            ;;
    esac
done

# make sure path is not empty
if [[ -z "$dir" ]]; then
    echo 'path is not valid'
    exit 1
fi

# make sure action is not empty
if [[ -z "$action" ]]; then
    usage
fi

# check if action is load
if [[ "$action" == 'load' ]]; then
    # make sure directory exists
    if [[ ! -d "$dir" ]]; then
        echo 'path is not valid'
        exit 1
    fi

    # load configurations
    for program in "${!configurations[@]}"; do
        val="${configurations[$program]}"
        path="$dir/$program.ini"
        if hash "$program" 2>/dev/null && [[ -f "$path" ]]; then
            echo "loading: '$val' < '$path'"
            dconf load "$val" < "$path"
        fi
    done
# check if action is dump
elif [[ "$action" == 'dump' ]]; then
    # create directory if it does not exist
    if [[ ! -d "$dir" ]]; then
        mkdir -p "$dir"
    fi

    # dump configurations
    for program in "${!configurations[@]}"; do
        val="${configurations[$program]}"
        path="$dir/$program.ini"
        if hash "$program" 2>/dev/null; then
            echo "dumping: '$val' > '$path'"
            dconf dump "$val" > "$path"
        fi
    done
fi
