#!/usr/bin/env bash

#
# ~/.bashrc
#

# if not running interactively, don't do anything
[[ $- != *i* ]] && return

#######
# SHOPT
#######

shopt -s autocd
shopt -ou histexpand
shopt -s cdspell
shopt -s checkwinsize
shopt -s direxpand
shopt -s dirspell
shopt -s extglob
shopt -s xpg_echo

#########
# ALIASES
#########

alias cmd='cmd.exe /C'
alias cp='cp -rv'
alias edit='$EDITOR'
alias ls='ls -AXC -lsh --color=always 2>/dev/null'
alias mkdir='mkdir -p'
alias python='python3'
alias rm='rm -rf'
alias rsync='rsync -rvP'
alias scp='scp -r'
alias ssh='ssh -o ServerAliveInterval=30 -o HostKeyAlgorithms=+ssh-rsa,ssh-dss'
alias ssh-keygen='ssh-keygen -t ed25519 -o -a 100'
alias sudo='sudo '

###########
# FUNCTIONS
###########

git_info() {
    local ahead
    local bits
	local branch
    local deleted
    local dirty
    local newfile
    local renamed
    local status
    local untracked

	branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [[ ! "${branch}" == "" ]]; then
		status=$(git status 2>&1 | tee)
		dirty=$(echo -n "${status}" 2> /dev/null \
		| grep 'modified:' &> /dev/null; echo "$?")
		untracked=$(echo -n "${status}" 2> /dev/null \
		| grep 'Untracked files' &> /dev/null; echo "$?")
		ahead=$(echo -n "${status}" 2> /dev/null \
		| grep 'Your branch is ahead of' &> /dev/null; echo "$?")
		newfile=$(echo -n "${status}" 2> /dev/null \
		| grep 'new file:' &> /dev/null; echo "$?")
		renamed=$(echo -n "${status}" 2> /dev/null \
		| grep 'renamed:' &> /dev/null; echo "$?")
		deleted=$(echo -n "${status}" 2> /dev/null \
		| grep 'deleted:' &> /dev/null; echo "$?")
		bits=''
		[[ "${renamed}" == '0' ]] && bits=">${bits}"
		[[ "${ahead}" == '0' ]] && bits="*${bits}"
		[[ "${newfile}" == '0' ]] && bits="+${bits}"
		[[ "${untracked}" == '0' ]] && bits="?${bits}"
		[[ "${deleted}" == '0' ]] && bits="x${bits}"
		[[ "${dirty}" == '0' ]] && bits="!${bits}"
		if [[ ! "${bits}" == '' ]]; then
			status=" ${bits}"
		else
			status=''
		fi
		echo "[${branch}${status}]"
	else
		echo ''
	fi
}

virtualenv_info() {
    local venv

    venv=''

    if [[ -n "$VIRTUAL_ENV" ]]; then
        venv="(${VIRTUAL_ENV##*/})"
    fi

    echo "$venv"
}

install() {
    install_cmd=''

    if hash apt 2>/dev/null; then
        install_cmd="(sudo apt install 2>/dev/null $@)"
    elif hash pacman 2>/dev/null; then
        install_cmd="(sudo pacman -S $@)"
    fi

    if hash flatpak 2>/dev/null; then
        install_cmd="$install_cmd || (sudo flatpak install flathub $@)"
    fi

    if [ "$WSL" = true ]; then
        install_cmd="$install_cmd || (cmd.exe /C choco install $@)"
    fi

    eval $install_cmd
}

uninstall() {
    uninstall_cmd=''

    if hash apt 2>/dev/null; then
        uninstall_cmd="(sudo apt purge 2>/dev/null $@ && sudo apt autoremove)"
    elif hash pacman 2>/dev/null; then
        uninstall_cmd="(sudo pacman -Rns 2>/dev/null $@)"
    fi

    if hash flatpak 2>/dev/null; then
        uninstall_cmd="$uninstall_cmd || (sudo flatpak uninstall --delete-data $@ && flatpak uninstall --unused)"
    fi

    if [ "$WSL" = true ]; then
        uninstall_cmd="$uninstall_cmd || (cmd.exe /C choco uninstall $@)"
    fi

    eval $uninstall_cmd
}

search() {
    if hash apt 2>/dev/null; then
        apt search "$@"
    fi

    if hash pacman 2>/dev/null; then
        pacman -Ss "$@"
    fi

    if hash flatpak 2>/dev/null; then
        flatpak search --columns=name,application,version "$@"
    fi

    if [ "$WSL" = true ]; then
        cmd.exe /C choco search "$@"
    fi
}

list() {
    if hash apt 2>/dev/null; then
        apt list --installed
    fi

    if hash pacman 2>/dev/null; then
        pacman -Q
    fi

    if hash flatpak 2>/dev/null; then
        flatpak list --app --columns=name,application,version
    fi

    if [ "$WSL" = true ]; then
        cmd.exe /C choco list --local-only
    fi
}

du() {
    command du -ah --max-depth=1 "$@" 2>/dev/null | sort -h
}

s3ls() {
    s3cmd ls "$@" | sort -k3 -h
}

crontab() {
    if [ $@ == "-e" ]; then
        eval $EDITOR ~/.crontab && command crontab ~/.crontab
    else
        command crontab $@
    fi
}

########
# PROMPT
########

# setup window title
PROMPT_COMMAND='echo -ne "\033]0;${PWD}\007"'

# setup PS1 command prompt
PS1="\\[\\e[1m\\]\\u@\\h\$(virtualenv_info):\\W\$(git_info)\
\\$\\[\\e[0m\\] "

# enable dir color support of ls
[[ "$TERM" != "dumb" ]] && eval "$(dircolors -b "$DIR_COLORS")"
